@echo off
@echo ----------------------------------------------------------------------
@echo script for granting READ_LOGS permission to Call Vibrator (Lite/Donate)
@echo                                          author: yutouji0917@gmail.com
@echo ----------------------------------------------------------------------
@echo  [*] Functions:
@echo      (1) grant READ_LOGS permission to Call Vibrator(Lite)
@echo      (2) grant READ_LOGS permission to Call Vibrator(Donation)
@echo  [*] Prerequisites:
@echo      (1) Install ADB driver for the device
@echo      (2) Turn on Android debugging
@echo      (3) Connect your device and PC with USB cable
@echo ----------------------------------------------------------------------
pause

@if not exist .\libs\adb.exe (goto FILE_FALSE_001)
@if not exist .\libs\AdbWinApi.dll (goto FILE_FALSE_002)
@if not exist .\libs\AdbWinUsbApi.dll (goto FILE_FALSE_003)

goto FILE_TRUE

:FILE_FALSE_001
@echo Error!! adb.exe not found !!
goto FILE_FALSE

:FILE_FALSE_002
@echo Error!! AdbWinApi.dll not found !!
goto FILE_FALSE

:FILE_FALSE_003
@echo Error!! AdbWinUsbApi.dll not found !!
goto FILE_FALSE

:FILE_FALSE
@echo Required files not found or corrupted, exit...
goto BATCH_END

:FILE_TRUE
echo Functions:
echo 1) Grant READ_LOGS permission to Call Vibrator(Lite)
echo 2) Grant READ_LOGS permission to Call Vibrator(Donate)
echo x) Exit
set /p type=Please select function��1 to x��: 
if %type% == 1 GOTO GRANT_LITE
if %type% == 2 GOTO GRANT_DONATION
if %type% == x GOTO EXIT

:GRANT_LITE
@echo Waiting for device...
@libs\adb.exe wait-for-device

@echo Granting READ_LOGS permission to Call Vibrator(Lite)...
@libs\adb.exe shell "pm grant io.github.yutouji0917.callvibrator.ad android.permission.READ_LOGS"
@echo permission granted
GOTO BATCH_END

:GRANT_DONATION
@echo Waiting for device...
@libs\adb.exe wait-for-device

@echo Granting READ_LOGS permission to Call Vibrator(Donate)...
@libs\adb.exe shell "pm grant io.github.yutouji0917.callvibrator.donate android.permission.READ_LOGS"
@echo permission granted

:BATCH_END
pause

:EXIT
exit